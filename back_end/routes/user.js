const express = require('express')
const router = express.Router()
const auth = require('../auth')
const UserController = require('../controllers/user')

router.post('/email-exists', (req, res) => {
    UserController.emailExists(req.body).then(result => res.send(result))
})

router.post('/', (req, res) => {
    UserController.register(req.body).then(result => res.send(result))
})

router.post('/login', (req, res) => {
    UserController.login(req.body).then(result => res.send(result))
})

router.post('/verify-google-token-id', async (req, res) => {
    res.send(await UserController.verifyGoogleTokenId(req.body.tokenId))
})

router.post('/add-category', auth.verify, (req, res) => {
    req.body.userId = auth.decode(req.headers.authorization).id
    UserController.addCategory(req.body).then(result => res.send(result))
})

router.post('/add-record', auth.verify, (req, res) => {
    req.body.userId = auth.decode(req.headers.authorization).id
    UserController.addRecord(req.body).then(result => res.send(result))
})

router.get('/details', auth.verify, (req, res) => {
    const user = auth.decode(req.headers.authorization)
    UserController.get({ userId: user.id }).then(user => res.send(user))
})

router.get('/get-categories', auth.verify, (req, res) => {
    const user = auth.decode(req.headers.authorization)
    UserController.getCategories({ userId: user.id }).then(user => res.send(user))
})

router.post('/get-most-recent-records', auth.verify, (req, res) => {
    req.body.userId = auth.decode(req.headers.authorization).id
    UserController.getMostRecentRecords(req.body).then(result => res.send(result))
})

router.post('/get-records-by-range', auth.verify, (req, res) => {
    req.body.userId = auth.decode(req.headers.authorization).id
    UserController.getRecordsByRange(req.body).then(result => res.send(result))
})

router.post('/get-records-breakdown-by-range', auth.verify, (req, res) => {
    req.body.userId = auth.decode(req.headers.authorization).id
    UserController.getRecordsBreakdownByRange(req.body).then(result => res.send(result))
})

router.post('/search-record', auth.verify, (req, res) => {
    req.body.userId = auth.decode(req.headers.authorization).id
    UserController.searchRecord(req.body).then(result => res.send(result))
})

module.exports = router