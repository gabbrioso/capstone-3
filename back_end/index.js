require('dotenv').config()

//dependencies setup
const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')
const userRoutes = require('./routes/user')
const port = process.env.PORT

//add whitelisted origins here
const corsOptions = {
	origin: ['http://localhost:3000'],
	optionsSuccessStatus: 200//for compatibility with older browsers
}

//database connection
mongoose.connection.once('open', () => console.log('Now connected to local MongoDB server.'))
mongoose.connect('mongodb+srv://gabbrioso:gab123@cluster0.zdsgg.mongodb.net/budget_tracker?retryWrites=true&w=majority', { 
    useNewUrlParser: true, 
    useUnifiedTopology: true 
})

//server setup
const app = express()

//bodyparser middleware
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(cors())

//add all the routes
app.use('/api/users', cors(corsOptions), userRoutes)

//server listening
app.listen(port, () => {
    console.log(`API is now online on port ${ port }`)
})