import { useContext } from 'react'
//import necessary bootstrap components
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
//import nextJS Link component for client-side navigation
import Link from 'next/link'



import UserContext from '../UserContext'

export default function NavBar() {
    //consume the UserContext and destructure it to access the user state from the context provider
    const { user } = useContext(UserContext)

    return (
        <header id="home" class="header">
		    <div class="navbar fixed-top">
                <Link href="/">
                    <a className="navbar-brand navbar-brand-img">
                        <img className="landing-logo" src="/logo2.png"/>
                        <img className="landing-logoB" src="/logo1.png"/> 
                    </a>
                </Link>

                {console.log(user)}
                {(user.email !== null)
                    ? <>
                    <div class="ml-auto text-right container-fluid text">
                        <nav>
                            <ul class="navbar-nav">
                                <Link href="/categories">
                                <a className="nav-link" role="button">Categories</a>
                                </Link>
                                <Link href="/records">
                                    <a className="nav-link" role="button">Records</a>
                                </Link>
                                <Link href="/analytics">
                                    <a className="nav-link" role="button">Analytics</a>
                                </Link>
                                <Link href="/logout">
                                    <a className="nav-link" role="button">Logout</a>
                                </Link>
                            </ul>
                        </nav>
                    </div>
                        
                    </>
                    : <>
                        <Link href="/login">
                            <a className="nav-link" role="button">
                                <img className="login-button" src="/login2.png"/>
                                <img className="login-buttonB" src="/login1.png"/> 
                            </a>
                        </Link>
                        {/* <Link href="/register">
                            <a className="nav-link" role="button">Register</a>
                        </Link> */}
                    </>
                }

            </div>
        </header>
    )
}
