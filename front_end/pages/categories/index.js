import { useState, useEffect } from 'react';
import { Table, Alert, Row, Col, Card, Button } from 'react-bootstrap'
import { useContext } from 'react';
import UserContext from '../../UserContext';
import Router from 'next/router';
import Link from 'next/link'

import AppHelper from '../../app-helper'




export default function index(){
    const [categories, setCategories] = useState([])

    useEffect(() => {
        let token = localStorage.getItem('token')

        fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/get-categories`, {
            headers: {
                Authorization: `Bearer ${token}`
            } 
        })
        .then(res => res.json())
        .then(data => {
            // console.log(data)
            if(data._id !== null){
                setCategories(data)
            }else{
                setCategories([])
            }
        })
    },[])
	
    return (
        <React.Fragment>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <Link href="/categories/new">
                <a className="btn" role="button">Add</a>
            </Link>
            {categories.length > 0
            ? <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Type</th>
                    </tr>
                </thead>
                <tbody>
                    {categories.map(category =>{
                        return(
                            <tr key={category._id}>  
                                <td>{category.name}</td>
                                <td>{category.type}</td>                                     
                            </tr>
                        )
                    })
                    }
                </tbody>
            </Table>
            :   <Alert variant="info">You have no categories yet.</Alert>
            }
        </React.Fragment>
    )
}