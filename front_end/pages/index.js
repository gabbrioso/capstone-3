import { useState, useContext } from 'react';
import { Card, Row, Col, Button } from 'react-bootstrap'
import { GoogleLogin } from 'react-google-login'
import Router from 'next/router'
import Swal from 'sweetalert2'

import UserContext from '../UserContext';
import AppHelper from '../app-helper'
import View from '../components/View'

import Link from 'next/link'

export default function index() {

    return (
        <React.Fragment>
            <section id="landing">
                <div class="landing-container">
                    <div class="landing-video">
                        <video autoplay muted loop id="myVideo">
                            <source src="./assets/images/landing-vid.mp4" type="video/mp4"/>
                        </video>
                        <h1 class="landing-text text-lg-left">
                            Design your<br/>Budget <img className="landing-text-img" src="/for-landing-text.png"/>
                        </h1>
                        <Link href="/register">
                            <button type="submit" class="btn btn-outline-danger landing-button my-3 hoverable">
                                Get Started
                                {/* <a className="nav-link" role="button"></a>  */}
                            </button>               
                        </Link>
                    </div>
                </div>
            </section>
        </React.Fragment>
    )
}