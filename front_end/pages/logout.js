import { useContext } from 'react';
import UserContext from '../UserContext';
import Router from 'next/router';
import { Row, Col, Card, Button } from 'react-bootstrap'
import View from '../components/View'
import AppHelper from '../app-helper'


// let token = localStorage.getItem('token')
// let name = ''

// if(token){
// 	const options = {
// 		headers: { Authorization: `Bearer ${token}` } 
// 	}

// 	fetch(`${ AppHelper.API_URL }/users/details`, options).then(AppHelper.toJSON).then(data => {
// 		console.log(data)
// 		name = data.name
// 		// setUser({ email: data.email, isAdmin: data.isAdmin })
// 	})
// }



export default function logout(){
	
	const {unsetUser, setUser} = useContext(UserContext)

	function logoutClick(){
		unsetUser();
		Router.push('/login')
	}

	return (
		<View title={ 'Logout' }>
			<Row className="justify-content-center">
				<Col xs md="6">
					<h3>Are you sure you want to logout?</h3>
					<Card>
						<Card.Header>Login Details</Card.Header>
						<Card.Body>
							<Button className="mb-2" variant="dark" type="submit" onClick={ logoutClick } block>Logout</Button>
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</View>
	)
}