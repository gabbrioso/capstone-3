import { useState, useEffect } from 'react';

import '../styles/globals.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import { UserProvider } from '../UserContext';
import AppHelper from '../app-helper';

import Navbar from '../components/NavBar'

function MyApp({ Component, pageProps }) {

	const [user, setUser] = useState({
		email: null
	})

	useEffect(() => {
		const accessToken  = localStorage.getItem('token')

		const options = {
            headers: { Authorization: `Bearer ${ accessToken }` } 
		}
		
		fetch(`${ AppHelper.API_URL }/users/details`, options).then(AppHelper.toJSON).then(data => {
			console.log(data)
			if(data.email){
				setUser({ email: data.email, userId: data.userId })
			}else{
				setUser({
					email: null
				})
			}	
		})
		
        
        
	}, [user.email])

	const unsetUser = () => {
		localStorage.clear();

		setUser({
			email: null
		})
	}

  return(
  	<UserProvider value={{user, setUser, unsetUser}}>
	  <Navbar />
	  	<Component {...pageProps} />
  	</UserProvider>
  )
}

export default MyApp
